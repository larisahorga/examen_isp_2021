package examen;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
public class s2 extends Frame {

    TextField textfield1;
    TextField textfield2;
    Button button;

    String temp;

    public s2() {

        setSize(600, 600);
        setLayout(new FlowLayout());

        textfield1 = new TextField(8);
        textfield1.setEditable(true);

        textfield2 = new TextField(8);
        textfield2.setEditable(false);
        button = new Button("Add text");

        add(textfield1);
        add(textfield2);
        add(button);




        addWindowListener( new WindowAdapter()
                           {
                               public void windowClosing(WindowEvent e)
                               {
                                   System.exit(0);
                               }
                           }
        );


        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textfield2.setText(textfield1.getText());
            }
        });
    }

    public static void main(String[] args) {

        s2 myGuiExampleObject = new s2();
        myGuiExampleObject.setVisible(true);


    }

}